package com.example.appli_mess;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.ArrayMap;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.appli_mess.Adapter.ListMessageAdapter;
import com.example.appli_mess.Controle.recupContact;
import com.example.appli_mess.Controle.requete_message;
import com.example.appli_mess.Controle.requete_personne;
import com.example.appli_mess.beans.Message;
import com.example.appli_mess.beans.Personne;
import com.example.appli_mess.beans.dernierMessage;
import com.example.appli_mess.connexion.Access1;
import com.example.appli_mess.lesInterfaces.Click_Sur_Elt;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Program_Activite extends AppCompatActivity implements Click_Sur_Elt {

    private ListMessageAdapter listmessage;
    RecyclerView recyclerView;
    public static recupContact contact;
    RecyclerView.LayoutManager layoutManager;
    List<dernierMessage> liste;
    Access1 access;
    TextView nouveau;
    private Click_Sur_Elt elt;
    public static List<String> numero_nom;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_program__activite);
        nouveau = (TextView) findViewById(R.id.id_mess_0);
        recupCont mescontact = new recupCont();
        mescontact.run();
        elt = this;
        recyclerView = (RecyclerView) findViewById(R.id.id_liste_mess);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        access = new Access1(getApplicationContext());
        //DatabaseReference reference =
        liste = new requete_message().getDernierMessage(access);
        listmessage = new ListMessageAdapter(liste, elt);
        recyclerView.setAdapter(listmessage);

        nouveau.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), Esp_Mess.class);
                intent.putExtra("contact", "");
                intent.putExtra("new", 1);
                intent.putExtra("nom", "sans nom");
                startActivity(intent);
            }
        });


    }

    @Override
    public void recyclerviewOnClick(int position) {

    }

    public class recupCont extends Thread{
        @Override
        public void run() {
            super.run();
            numero_nom = new ArrayList<>();
            numero_nom.add("contact");
            contact = new recupContact(getApplicationContext());
            for (String key : contact.Mescontacts.keySet()){

                numero_nom.add(contact.Mescontacts.get(key)+"-"+key);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        liste = new requete_message().getDernierMessage(access);
        listmessage = new ListMessageAdapter(liste, elt);
        recyclerView.setAdapter(listmessage);
    }
}
