package com.example.appli_mess;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;

import com.andrognito.patternlockview.PatternLockView;
import com.andrognito.patternlockview.listener.PatternLockViewListener;
import com.andrognito.patternlockview.utils.PatternLockUtils;
import com.example.appli_mess.beans.Personne;

import java.util.List;

public class Entrer_Schema extends AppCompatActivity {

    public final static Personne prop = new Personne("ppo", "tuf", "0000", "max");
    PatternLockView pattern0;
    private PatternLockViewListener mPatternLockViewListener;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_entrer__schema);
        pattern0 = (PatternLockView) findViewById(R.id.schema_princ);
        mPatternLockViewListener = new PatternLockViewListener() {
            @Override
            public void onStarted() {

            }

            @Override
            public void onProgress(List<PatternLockView.Dot> progressPattern) {

            }

            @Override
            public void onComplete(List<PatternLockView.Dot> pattern) {
                SharedPreferences preferences = getSharedPreferences("PEPS1", 0);
                String schem1 = preferences.getString("schema1", "0");
                String schem2 = preferences.getString("schema2", "0");
                if (schem1.equals(PatternLockUtils.patternToString(pattern0, pattern))) {
                    System.out.println("valeur de password 1 correcte ");
                    Intent intent = new Intent(getApplicationContext(), Program_Activite.class);
                    startActivity(intent);
                    finish();
                } else if (schem2.equals(PatternLockUtils.patternToString(pattern0, pattern))){
                    Intent intent = new Intent(getApplicationContext(), Program_Activite.class);
                    startActivity(intent);
                    finish();
                    System.out.println("valeur de password 2 correcte ");
                } else {
                    System.out.println("valeur de password incorrecte ");
                }

            }

            @Override
            public void onCleared() {

            }
        };
        pattern0.addPatternLockListener(mPatternLockViewListener);
    }
}
