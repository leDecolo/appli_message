package com.example.appli_mess;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.appli_mess.Controle.recupContact;
import com.example.appli_mess.Controle.requete_message;
import com.example.appli_mess.beans.Message;
import com.example.appli_mess.beans.dernierMessage;
import com.example.appli_mess.connexion.Access1;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Creer extends AppCompatActivity {

    Button b1, b2, b3;
    Access1 access;
    requete_message requete;
    Map<String, Date> num_date;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_creer);
        num_date = new HashMap<String, Date>();
        access = new Access1(getApplicationContext());
        requete = new requete_message();

        b1 = (Button) findViewById(R.id.b1);
        b2 = (Button) findViewById(R.id.b2);
        b3 = (Button) findViewById(R.id.b3);

        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), Schema1.class);
                startActivity(intent);
            }
        });

        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), Schema2.class);
                startActivity(intent);
            }
        });

        b3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                creation_db();
                demandePermission();
                charger_sms(getApplication().getContentResolver(), requete, access);


                Intent intent = new Intent(getApplicationContext(), Program_Activite.class);
                startActivity(intent);
                finish();
            }
        });
    }

    public void charger_sms(ContentResolver resolver, requete_message access1, Access1 access2){
        List<String> listM = new ArrayList<>();
        recupContact mesCont = new recupContact(getApplicationContext());
        Map<String, String> liste = new HashMap<>();
        String receive_sms = "content://sms/inbox";
        String send_sms = "content://sms/sent";
        Cursor cursor1 = resolver.query(Uri.parse(receive_sms), null, null, null, null);
        Cursor cursor2 = resolver.query(Uri.parse(send_sms), null, null, null, null);
        //Boolean verif = cursor.moveToFirst();
        int i= cursor1.getCount(), j=cursor2.getCount();
        if (cursor2 == null)
            Toast.makeText(getApplicationContext(), "Impossible de recuperer les messages", Toast.LENGTH_SHORT);

        else if (cursor1.moveToLast() && cursor2.moveToLast()){
            do {
                Message m1 = null;
                Message m2 = null;
                //Ajout des messages envoyes
                if (j>0){
                    String numero1 = cursor2.getString(cursor2.getColumnIndex("address")).replaceAll("-", "");
                    System.out.println("message envoye "+cursor2.getString(cursor2.getColumnIndex("address")));
                    String numero2 = numero1.replaceAll(" ", "");
                    numero2 = numero2.replaceAll("[+]237", "");
                    System.out.println("message envoye1 "+numero2);
                    DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String indexDate = cursor2.getString(cursor2.getColumnIndex("date"));
                    Date date = new Date(Long.parseLong(indexDate));
                    indexDate = format.format(date);
                    m1 = new Message(cursor2.getString(cursor2.getColumnIndex("body"))
                            ,numero2
                            , "1"
                            , indexDate, 0);
                    //ajout du message dans la base de donnee
                    access1.ajout_message(m1, access2);

                    if(!listM.contains(numero2)) {
                        //Sauvegarde de l'addresse telephonique
                        listM.add(numero2);

                        //Ajout de la correspondance numero->message de fin(ou dernier message envoye ou recu)
                        liste.put(numero2, cursor2.getString(cursor2.getColumnIndex("body")));
                        num_date.put(numero2, date);
                    } else if (num_date.get(numero2).before(date)){
                        //Si l'adresse a deja ete ajoute, remplacer le message de fin
                        liste.put(numero2, cursor2.getString(cursor2.getColumnIndex("body")));
                        num_date.put(numero2, date);
                    }
                    j--;
                }
                //Ajout des messages recus
                if(i>0){

                    String numero1 = cursor1.getString(cursor1.getColumnIndex("address")).replaceAll("-", "");
                    System.out.println("message recu "+cursor1.getString(cursor1.getColumnIndex("address")));
                    String numero2 = numero1.replaceAll(" ", "");
                    numero2 = numero2.replaceAll("[+]237", "");
                    System.out.println("message recu2 "+numero2);
                    //Recuperation des messages recus sur par l'utilisateur
                    DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String indexDate = cursor1.getString(cursor1.getColumnIndex("date"));
                    Date date = new Date(Long.parseLong(indexDate));
                    indexDate = format.format(date);
                    m2 = new Message(cursor1.getString(cursor1.getColumnIndex("body"))
                            ,numero2, "0", indexDate, 0);

                    //Ajout dans ma base de donnee des messages recus
                    access1.ajout_message(m2, access2);
                    if(!listM.contains(numero2)) {
                        listM.add(numero2);
                        liste.put(numero2, cursor1.getString(cursor1.getColumnIndexOrThrow("body")));
                        num_date.put(numero2, date);
                    } else if (num_date.get(numero2).before(date)) {
                        liste.put(numero2, cursor1.getString(cursor1.getColumnIndexOrThrow("body")));
                        num_date.put(numero2, date);
                    }
                    i--;
                }

                cursor1.moveToPrevious();
                cursor2.moveToPrevious();

                //Toast.makeText(getApplicationContext(), "nom: "+ cursor.getString(cursor.getColumnIndexOrThrow("address"))
                //+ " message: "+cursor.getString(cursor.getColumnIndexOrThrow("body")), Toast.LENGTH_LONG);
            }while ( i>0 || j>0);
        }

            for(Map.Entry entree : liste.entrySet()){
            dernierMessage dm ;
            if(mesCont.Mescontacts.containsKey(entree.getKey().toString()))
                dm = new dernierMessage(entree.getKey().toString(), entree.getValue().toString(), "0",  0, 0, mesCont.Mescontacts.get(entree.getKey().toString()));
            else
                dm = new dernierMessage(entree.getKey().toString(), entree.getValue().toString(), "0",  0, 0, "sans nom");
            access1.ajout_dernier_mess(dm, access2);

        }
        }

    public void creation_db(){
        access = new Access1(getApplicationContext());
    }

    public void demandePermission(){
        if(ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED){
            String[] permission = new String[]{Manifest.permission.SEND_SMS
                    , Manifest.permission.READ_SMS
                    , Manifest.permission.READ_CONTACTS};

            ActivityCompat.requestPermissions(Creer.this, permission, 2);
            Toast.makeText(getApplicationContext(), "Permissions accordees", Toast.LENGTH_SHORT).show();
        }

    }
}
