package com.example.appli_mess.Controle;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.appli_mess.Entrer_Schema;
import com.example.appli_mess.beans.Message;
import com.example.appli_mess.beans.Personne;
import com.example.appli_mess.beans.dernierMessage;
import com.example.appli_mess.connexion.Access1;

import java.util.ArrayList;
import java.util.List;

public class requete_message {

    public void ajout_message(Message m, Access1 access){
        SQLiteDatabase db = access.getWritableDatabase();
        ContentValues content = new ContentValues();
        content.put("contenu", m.getContenu());
        content.put("adresse", m.getAdresse());
        content.put("direction", m.getDirection());
        content.put("date", m.getDate());
        content.put("lecture", m.getLecture());
        db.insert("message", null, content);
        db.close();
    }

    public void ajout_dernier_mess(dernierMessage m, Access1 access){
        SQLiteDatabase db = access.getWritableDatabase();
        ContentValues content = new ContentValues();
        content.put("adresse", m.getAdresse());
        content.put("contenu", m.getContenu());
        content.put("date", m.getDate());
        content.put("nonLu", m.getNbreNonLu());
        content.put("securite", m.getSecurite());
        content.put("nom", m.getNom());

        db.insert("dernierMessage", null, content);
        db.close();
    }

    public List<dernierMessage> getDernierMessage(Access1 access){
        List<dernierMessage> listMessage = new ArrayList<>();
        SQLiteDatabase db = access.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM dernierMessage ORDER BY date", null );
        if (cursor != null && cursor.getCount() != 0){
            cursor.moveToFirst();
            do {
                dernierMessage mes1 = new dernierMessage( cursor.getInt(0)
                        , cursor.getString(1)
                        , cursor.getString(2)
                        , cursor.getString(3)
                        , cursor.getInt(4)
                        , cursor.getInt(5)
                        , cursor.getString(6));
                listMessage.add(mes1);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return listMessage;
    }

    public List<Message> getCont(Access1 access){
        List<Message> listMessage = new ArrayList<>();
        SQLiteDatabase db = access.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT DISTINCT emetteur, contenu FROM message", null );
        if (cursor.moveToFirst()){
            do {
                Message mes1 = new Message( cursor.getInt(0), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4));
                listMessage.add(mes1);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return listMessage;
    }

    public List<Message> getMessage(Access1 access, String numero){
        List<Message> listMessage = new ArrayList<>();
        SQLiteDatabase db = access.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM message WHERE adresse = ? ORDER BY date", new String[]{numero} );
        if (cursor.moveToFirst()){
            do {
                Message mes1 = new Message( cursor.getInt(0), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4));
                listMessage.add(mes1);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return listMessage;
    }

    public void delete_contenu(Access1 access){
        SQLiteDatabase db = access.getWritableDatabase();
        db.close();

    }

    public int verifDernierMessage(Access1 access, String numero){
        int result = 0;
        SQLiteDatabase db = access.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM dernierMessage WHERE adresse = ?", new String[]{numero});
        if(cursor != null && cursor.getCount() != 0){
            result = 1;
        } else {
            result = 0;
        }
        cursor.close();
        db.close();
        return result;
    }

    public int modifDernierMessage(String numero, String message, String date, Access1 access){
        int result = 0;
        SQLiteDatabase db = access.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("contenu", message);
        values.put("date", date);
        result = db.update("dernierMessage", values, "adresse = ?", new String[]{numero});
        db.close();
        return result;
    }

    public int supprimer_message(Access1 access, String date, String message, String adresse, int position, int taille){
        SQLiteDatabase db = access.getWritableDatabase();
        SQLiteDatabase db1 = access.getReadableDatabase();

        ContentValues value = new ContentValues();
        value.put("date", date);
        value.put("contenu", message);
        value.put("adresse", adresse);
        int result = db.delete("message", "date = ? AND contenu = ? AND adresse = ?", new String[]{date, message, adresse});
        int t = taille;
        int pos = position+1;
        if (pos == taille){
 //           int result2 = db.delete("dernierMessage", "adresse = ? AND contenu = ? AND date = ?", new String[]{date, message, adresse});
            Cursor cursor = db1.rawQuery("SELECT * FROM message WHERE adresse = ? ORDER BY date DESC LIMIT 0,1", new String[]{adresse} );
            if (cursor.moveToFirst()){
                ContentValues content = new ContentValues();
                content.put("contenu", cursor.getString(1));
                content.put("date", cursor.getString(4));

                int relsult1 = db.update("dernierMessage", content, "adresse = ? ", new String[]{adresse});
            }

        }
        return result;

    }
}
