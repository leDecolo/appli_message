package com.example.appli_mess.Controle;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.Contacts;
import android.provider.ContactsContract;
import android.util.ArrayMap;

import java.util.Map;

public class recupContact {
    public Map<String, String> Mescontacts;
    public recupContact(Context context){
        // notre tableau de contact
        Mescontacts = new ArrayMap<>();
        // instance qui permet d'acceder au contenu d'autre application
        ContentResolver ConnectApp = context.getContentResolver();
        Uri uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        String[] projection = new String[] {ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME_ALTERNATIVE
                                            , ContactsContract.CommonDataKinds.Phone.NUMBER};
        // on récupere les contacts dans un curseur
        Cursor cursor = ConnectApp.query(uri, projection, null, null, null);

        if (cursor.moveToFirst()) {
            do {
                Mescontacts.put(cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER))
                                , cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME_ALTERNATIVE)));
            } while (cursor.moveToNext());
        }
    }
    }
