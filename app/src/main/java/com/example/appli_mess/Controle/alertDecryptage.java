package com.example.appli_mess.Controle;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatDialogFragment;

import com.example.appli_mess.R;

public class alertDecryptage extends AppCompatDialogFragment {

    RadioGroup radio;
    LinearLayout esp_cesar, esp_vig;
    EditText val_cesar, val_vig;
    TextView mes_cesar, mes_vig;
    alertDecryptageListener listener;
    int choix = 1;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.alert_decryptage, null);
        builder.setView(view)
                .setTitle(R.string.titre2)
                .setNegativeButton(R.string.choix_negatif, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .setPositiveButton(R.string.choix_positif, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String valeur1 = val_vig.getText().toString();
                        String valeur2 = val_cesar.getText().toString();
                        if(choix == 1 && !val_vig.getText().toString().trim().equals("")){
                            String type = "vigenere";
                            String valeur = val_vig.getText().toString();
                            listener.applyTexts(type, valeur);
                        }
                        else if(choix == 2 && !val_cesar.getText().toString().trim().equals("")){
                            String type = "cesar";
                            String valeur = val_cesar.getText().toString();
                            listener.applyTexts(type, valeur);
                        } else {
                            String type = "neant";
                            String valeur = "";
                            listener.applyTexts(type, valeur);
                        }

                    }
                });

        radio = (RadioGroup) view.findViewById(R.id.list_choix_dechif);
        esp_cesar = (LinearLayout) view.findViewById(R.id.esp_val_cesar1);
        esp_vig = (LinearLayout) view.findViewById(R.id.esp_val_vig1);
        val_cesar = (EditText) view.findViewById(R.id.valeur_cesar1);
        val_vig = (EditText) view.findViewById(R.id.valeur_vigenere1);
        mes_cesar = (TextView) view.findViewById(R.id.id_commentaire_cesar1);
        mes_vig = (TextView) view.findViewById(R.id.id_commentaire_vigenere1);

        radio.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.decrypt_vigenere){
                    esp_cesar.setVisibility(View.GONE);
                    esp_vig.setVisibility(View.VISIBLE);
                    mes_cesar.setVisibility(View.GONE);
                    mes_vig.setVisibility(View.VISIBLE);
                    choix = 1;


                }

                if (checkedId == R.id.decrypt_cesar){
                    esp_cesar.setVisibility(View.VISIBLE);
                    esp_vig.setVisibility(View.GONE);
                    mes_cesar.setVisibility(View.VISIBLE);
                    mes_vig.setVisibility(View.GONE);
                    choix = 2;

                }
            }
        });


        return builder.create();

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (alertDecryptageListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()+" impossible de caster");
        }

    }

    public interface alertDecryptageListener{
        void applyTexts(String type, String valeur);
    }
}
