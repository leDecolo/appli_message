package com.example.appli_mess.Controle;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

public class Decryptage {

public static String decryptage_cesar(String texte, int dec){
    String resultat = "";

    //j'initialise mon alphabet
    String alpha = "abcdefghijklmnopqrstuvwxyz";
    //j'initialise mon expression reguliere pour les alphabet en minuscule
    String regex_min = "a|b|c|d|e|f|g|h|i|j|k|l|m|n|o|p|q|r|s|t|u|v|w|x|y|z";
    //j'initialise mon expression reguliere pour les alphabet en majuscule
    String regex_maj = "A|B|C|D|E|F|G|H|I|J|K|L|M|N|O|P|Q|R|S|T|U|V|W|X|Y|Z";
    //je cree une correspondance alphabet --> numero
    Map<Character, Integer> alpha_num = new HashMap<>();
    //je cree une correspondance numero --> alphabet
    Map<Integer, Character> num_alpha = new HashMap<>();

    //je remplis les differentes correspondances
    for (int i=0; i<alpha.length(); i++){
        alpha_num.put(alpha.charAt(i), i);
        num_alpha.put(i, alpha.charAt(i));
    }

    for (int i=0; i<texte.length(); i++){


        if (Pattern.matches(regex_min, ""+texte.charAt(i)) || Pattern.matches(regex_maj, ""+texte.charAt(i))){
            char c = (texte.charAt(i)+"").toLowerCase().charAt(0);
            int niv = alpha_num.get(c);
            int decalage = niv - dec;
            if (decalage < 0 ){
                do {
                    decalage = 26 + decalage;
                }while (decalage < 0);
            }

            char val = num_alpha.get(decalage);
            if (Pattern.matches(regex_maj, ""+texte.charAt(i))) {
                val = (val + "").toUpperCase().charAt(0);
                resultat = resultat + val;
            } else
                resultat = resultat+val;

        } else if(Pattern.matches("0|1|2|3|4|5|6|7|8|9", ""+texte.charAt(i))){
            int v = Integer.parseInt(texte.charAt(i)+"");
            int decalage = v-dec;
            if (decalage < 0){
                do {
                    decalage = 10 + decalage;
                }while (decalage < 0);
            }
            resultat = resultat + decalage;
        } else if ((texte.charAt(i)+"").equals("&") && (texte.charAt(i+1)+"").equals("%") && (texte.charAt(i+2)+"").equals("&")){
            resultat = resultat+" ";
            i = i+2;
        } else {
            resultat = resultat + texte.charAt(i);
        }



    }

    return resultat;
}



public static String decryptage_vigenere(String message, String mot, char[][] decodage){
    for (int i=0; i<26; i++){
        System.out.println("\n");
        for (int j=0; j<26; j++){
            System.out.print("  "+decodage[i][j]);
        }
    }
    // mot contenant le resultat
    String resultat = "";
    //correspondance caractere entier
    Map<Character, Integer> alpha_num = new HashMap<>();
    //correspondance entier charactere
    Map<Integer, Character> num_alpha = new HashMap<>();
    //liste de tout l'alphabet
    String alpha = "abcdefghijklmnopqrstuvwxyz";

    //expression reguliere de l'alphabet en minuscule
    String regex_min = "a|b|c|d|e|f|g|h|i|j|k|l|m|n|o|p|q|r|s|t|u|v|w|x|y|z";

    //expression reguliere de l'alphabet en majuscule
    String regex_maj = "A|B|C|D|E|F|G|H|I|J|K|L|M|N|O|P|Q|R|S|T|U|V|W|X|Y|Z";

    //remplissage des correspondance
    for (int i = 0; i<26; i++){
        num_alpha.put(i, alpha.charAt(i));
        alpha_num.put(alpha.charAt(i), i);
    }

    //
    String correspondance = "";
    int niveau = 0;

    // ici je fais correspondre chaque mot a une lettre du texte a code
    for (int t = 0; t<message.length(); t++){

        if (Pattern.matches(regex_min, message.charAt(t)+"") || Pattern.matches(regex_maj, message.charAt(t)+"")){
            //char c = (message.charAt(t)+"").toLowerCase().charAt(0);
            char c1 = mot.charAt(niveau);
                /*
                if (Pattern.matches(regex_min, mot.charAt(niveau)+"") || Pattern.matches(regex_maj, mot.charAt(niveau)+""))
                    c1 = (mot.charAt(niveau)+"").toLowerCase().charAt(0);*/
            while ((!Pattern.matches(regex_min, ""+mot.charAt(niveau)) || Pattern.matches(regex_maj, ""+mot.charAt(niveau)))
                    && (Pattern.matches(regex_min, ""+mot.charAt(niveau)) || !Pattern.matches(regex_maj, ""+mot.charAt(niveau)))){
                if (niveau == mot.length())
                    niveau = 0;
                niveau++;
            }
            if(Pattern.matches(regex_maj, ""+message.charAt(t))){
                correspondance = correspondance + (mot.charAt(niveau)+"").toUpperCase();
                niveau++;
            }

            else {
                correspondance = correspondance + mot.charAt(niveau);
                niveau++;
            }

        } else {
            correspondance = correspondance + " ";
        }

        if (niveau == mot.length())
            niveau = 0;
    }
    System.out.println(correspondance);


    for (int t = 0; t<message.length(); t++){
        if (Pattern.matches(regex_min, ""+message.charAt(t)) || Pattern.matches(regex_maj, ""+message.charAt(t))){

            char m = (message.charAt(t)+"").toLowerCase().charAt(0);
            char m1 = (correspondance.charAt(t)+"").toLowerCase().charAt(0);

            int chiffre1 = alpha_num.get(m);
            int chiffre2 = alpha_num.get(m1);

            char c = decodage[chiffre2][chiffre1];

            System.out.println("Correspondance "+m+"-"+m1+"-->"+c+"\n");

            if (Pattern.matches(regex_maj, ""+message.charAt(t))){
                c = (c+"").toUpperCase().charAt(0);
                resultat = resultat + c;
            }else
                resultat = resultat + c;

        } else if ((message.charAt(t)+"").equals("&") && (message.charAt(t+1)+"").equals("%")){
            resultat = resultat+" ";
            t = t+2;
        } else if(Pattern.matches("0|1|2|3|4|5|6|7|8|9", ""+message.charAt(t))){
            resultat = resultat + message.charAt(t);
        } else
            resultat = resultat + message.charAt(t);
    }
    System.out.println(resultat);
    return resultat;
}
}
