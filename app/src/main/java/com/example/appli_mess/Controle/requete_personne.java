package com.example.appli_mess.Controle;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.appli_mess.beans.Personne;
import com.example.appli_mess.connexion.Access1;

import java.util.ArrayList;
import java.util.List;

public class requete_personne {
    Access1 access;

    public requete_personne(Context context) {
        access = new Access1(context);
    }

    public void addPersonne(Personne pers){
        SQLiteDatabase db = access.getWritableDatabase();
        ContentValues value = new ContentValues();
        value.put("numero", pers.getNumero());
        value.put("nom", pers.getNom());
        value.put("prenom", pers.getPrenom());
        value.put("sexe", pers.getSexe());
        db.insert("person", null, value );
        db.close();

    }

    public Personne getPerson(String i){
        SQLiteDatabase db = access.getReadableDatabase();
        Cursor cursor = db.query("person", new String[]{"numero", "nom", "prenom", "sexe"}, "numero = ?",
                new String[]{i}, null, null, null);
        if (cursor != null){
            cursor.moveToFirst();
        }
        Personne pers = new Personne(cursor.getString(1), cursor.getString(2), cursor.getString(0), cursor.getString(3));
        return pers;
    }

    public List<Personne> listPerson(){
        List<Personne> listP = new ArrayList<>();
        String req = "SELECT * FROM person";
        SQLiteDatabase bd = access.getReadableDatabase();
        Cursor cursor = bd.rawQuery(req, null);
        if(cursor.moveToFirst()){
            do {
                Personne p = new Personne(cursor.getString(1), cursor.getString(2), cursor.getString(0), cursor.getString(3));
                listP.add(p);
            } while (cursor.moveToNext());
        }
        return listP;

    }

}
