package com.example.appli_mess.Controle;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

public class Cryptage {

    public static String Cryptage_cesar(String text, int dec, Map<Character, Integer> alpha_num, Map<Integer, Character> num_alpha){
        String resultat = "";
        //j'initialise mon alphabet
        String alpha = "abcdefghijklmnopqrstuvwxyz";
        //j'initialise mon expression reguliere pour les alphabet en minuscule
        String regex_min = "a|b|c|d|e|f|g|h|i|j|k|l|m|n|o|p|q|r|s|t|u|v|w|x|y|z";
        //j'initialise mon expression reguliere pour les alphabet en majuscule
        String regex_maj = "A|B|C|D|E|F|G|H|I|J|K|L|M|N|O|P|Q|R|S|T|U|V|W|X|Y|Z";
        alpha_num = new HashMap<>();
        num_alpha = new HashMap<>();
        for (int i=0; i<alpha.length(); i++){
            alpha_num.put(alpha.charAt(i), i);
            num_alpha.put(i, alpha.charAt(i));
        }

        for (int i=0; i<text.length(); i++){
            if (Pattern.matches(regex_min, ""+text.charAt(i)) || Pattern.matches(regex_maj, ""+text.charAt(i))){
                char c = (text.charAt(i)+"").toLowerCase().charAt(0);
                int niv = alpha_num.get(c);
                int decalage = niv + dec;
                if (decalage > 25 ){
                    do {
                        decalage = decalage - 26;
                    }while (decalage > 25);
                }

                char val = num_alpha.get(decalage);
                if (Pattern.matches(regex_maj, ""+text.charAt(i))) {
                    val = (val + "").toUpperCase().charAt(0);
                    resultat = resultat + val;
                } else
                    resultat = resultat+val;

            } else if(Pattern.matches("0|1|2|3|4|5|6|7|8|9", ""+text.charAt(i))){
                int v = Integer.parseInt(text.charAt(i)+"");
                int decalage = v+dec;
                if (decalage > 9){
                    do {
                        decalage = decalage - 10;
                    }while (decalage > 9);
                }
                resultat = resultat + decalage;
            } else if ((text.charAt(i)+"").equals(" ")){
                resultat = resultat+"&%&";
            } else {
                resultat = resultat + text.charAt(i);
            }

        }

        return resultat;
    }




    public static String Cryptage_Vigenere(String message, String mot, char[][] codage ){
        String resultat = "";
        Map<Character, Integer> alpha_num = new HashMap<>();
        Map<Integer, Character> num_alpha = new HashMap<>();
        String alpha = "abcdefghijklmnopqrstuvwxyz";
        String regex_min = "a|b|c|d|e|f|g|h|i|j|k|l|m|n|o|p|q|r|s|t|u|v|w|x|y|z";
        String regex_maj = "A|B|C|D|E|F|G|H|I|J|K|L|M|N|O|P|Q|R|S|T|U|V|W|X|Y|Z";

        for (int i = 0; i<26; i++){
            num_alpha.put(i, alpha.charAt(i));
            alpha_num.put(alpha.charAt(i), i);
        }

        String correspondance = "";
        int niveau = 0;

        for (int t = 0; t<message.length(); t++){

            if (Pattern.matches(regex_min, message.charAt(t)+"") || Pattern.matches(regex_maj, message.charAt(t)+"")){
                //char c = (message.charAt(t)+"").toLowerCase().charAt(0);
                char c1 = mot.charAt(niveau);
                /*
                if (Pattern.matches(regex_min, mot.charAt(niveau)+"") || Pattern.matches(regex_maj, mot.charAt(niveau)+""))
                    c1 = (mot.charAt(niveau)+"").toLowerCase().charAt(0);*/
                while ((!Pattern.matches(regex_min, ""+mot.charAt(niveau)) || Pattern.matches(regex_maj, ""+mot.charAt(niveau)))
                        && (Pattern.matches(regex_min, ""+mot.charAt(niveau)) || !Pattern.matches(regex_maj, ""+mot.charAt(niveau)))){
                    if (niveau == mot.length())
                        niveau = 0;
                    niveau++;
                }
                if(Pattern.matches(regex_maj, ""+message.charAt(t))){
                    correspondance = correspondance + (mot.charAt(niveau)+"").toUpperCase();
                    niveau++;
                }

                else {
                    correspondance = correspondance + mot.charAt(niveau);
                    niveau++;
                }

            } else {
                correspondance = correspondance + " ";
            }

            if (niveau == mot.length())
                niveau = 0;
        }
        System.out.println(correspondance);

        for (int t = 0; t<message.length(); t++){
            if (Pattern.matches(regex_min, ""+message.charAt(t)) || Pattern.matches(regex_maj, ""+message.charAt(t))){

                char m = (message.charAt(t)+"").toLowerCase().charAt(0);
                char m1 = (correspondance.charAt(t)+"").toLowerCase().charAt(0);
                int chiffre1 = alpha_num.get(m);
                int chiffre2 = alpha_num.get(m1);

                char c = codage[chiffre1][chiffre2];
                System.out.println("Correspondance "+m+"-"+m1+"-->"+c+"\n");

                if (Pattern.matches(regex_maj, ""+message.charAt(t))){
                    c = (c+"").toUpperCase().charAt(0);
                    resultat = resultat + c;
                }else
                    resultat = resultat + c;

            } else if ((message.charAt(t)+"").equals(" ")){
                resultat = resultat+"&%&";
            } else if(Pattern.matches("0|1|2|3|4|5|6|7|8|9", ""+message.charAt(t))){
                resultat = resultat + message.charAt(t);
            } else
                resultat = resultat + message.charAt(t);
        }
        System.out.println(resultat);
        return resultat;
    }
}
