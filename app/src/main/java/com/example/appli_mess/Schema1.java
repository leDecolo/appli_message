package com.example.appli_mess;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;

import com.andrognito.patternlockview.PatternLockView;
import com.andrognito.patternlockview.listener.PatternLockViewListener;
import com.andrognito.patternlockview.utils.PatternLockUtils;

import java.util.List;

public class Schema1 extends AppCompatActivity {

    PatternLockView pattern1;
    private PatternLockViewListener mPatternLockViewListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schema1);
        pattern1 = (PatternLockView) findViewById(R.id.schema1);
        mPatternLockViewListener = new PatternLockViewListener() {
            @Override
            public void onStarted() {

            }

            @Override
            public void onProgress(List<PatternLockView.Dot> progressPattern) {

            }

            @Override
            public void onComplete(List<PatternLockView.Dot> pattern) {
                Log.d(getClass().getName(), "Pattern progress: " +
                        PatternLockUtils.patternToString(pattern1, pattern));
                SharedPreferences preferences = getSharedPreferences("PEPS1", 0);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString("password", PatternLockUtils.patternToString(pattern1, pattern));
                editor.putString("schema1", PatternLockUtils.patternToString(pattern1, pattern));
                editor.apply();
                Intent intent = new Intent(getApplicationContext(), Creer.class);
                startActivity(intent);
                finish();
            }

            @Override
            public void onCleared() {

            }
        };

        pattern1.addPatternLockListener(mPatternLockViewListener);
    }
}
