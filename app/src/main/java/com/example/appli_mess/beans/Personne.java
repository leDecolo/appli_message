package com.example.appli_mess.beans;

public class Personne {



    public String getNom() {
        return nom;
    }

    public Personne(String nom, String prenom, String numero, String sexe) {
        this.nom = nom;
        this.prenom = prenom;
        this.numero = numero;
        this.sexe = sexe;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getSexe() {
        return sexe;
    }

    public void setSexe(String sexe) {
        this.sexe = sexe;
    }

    private String nom, prenom, numero;
    private String sexe;

}
