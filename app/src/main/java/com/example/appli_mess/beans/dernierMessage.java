package com.example.appli_mess.beans;

import java.util.Date;

public class dernierMessage {
    private int numero;
    private String nom;
    private String adresse;
    private String contenu;
    private String date;
    private int nbreNonLu;
    private int securite;

    public dernierMessage(int numero, String adresse, String contenu, String date, int nonlu, int securite, String nom) {
        this.numero = numero;
        this.adresse = adresse;
        this.contenu = contenu;
        this.date = date;
        this.nbreNonLu = nonlu;
        this.securite = securite;
        this.nom = nom;
    }

    public dernierMessage(String adresse, String contenu, String date, int nbreNonLu, int securite, String nom) {
        this.adresse = adresse;
        this.contenu = contenu;
        this.date = date;
        this.nbreNonLu = nbreNonLu;
        this.securite = securite;
        this.nom = nom;
    }

    public dernierMessage(String adresse, String contenu, int nbreNonLu) {
        this.adresse = adresse;
        this.contenu = contenu;
        this.nbreNonLu = nbreNonLu;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getSecurite() {
        return securite;
    }

    public void setSecurite(int securite) {
        this.securite = securite;
    }

    public int getNbreNonLu() {
        return nbreNonLu;
    }

    public void setNbreNonLu(int nbreNonLu) {
        this.nbreNonLu = nbreNonLu;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getContenu() {
        return contenu;
    }

    public void setContenu(String contenu) {
        this.contenu = contenu;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
