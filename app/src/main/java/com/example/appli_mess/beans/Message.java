package com.example.appli_mess.beans;

public class Message {
    private int numero;
    private String contenu;
    private String adresse;
    private String direction;
    private String date;
    private int lecture;
    private int crypter;

    public Message(int numero, String contenu, String adresse, String direction, String date) {
        this.numero = numero;
        this.contenu = contenu;
        this.adresse = adresse;
        this.direction = direction;
        this.date = date;
    }

    public Message(String contenu, String adresse, String direction, String date, int lecture) {
        this.contenu = contenu;
        this.adresse = adresse;
        this.direction = direction;
        this.date = date;
        this.lecture = lecture;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public int getLecture() {
        return lecture;
    }

    public void setLecture(int lecture) {
        this.lecture = lecture;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getContenu() {
        return contenu;
    }

    public void setContenu(String contenu) {
        this.contenu = contenu;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setEmetteur(String adresse) {
        this.adresse = adresse;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

}
