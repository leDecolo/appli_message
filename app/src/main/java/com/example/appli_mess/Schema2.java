package com.example.appli_mess;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;

import com.andrognito.patternlockview.PatternLockView;
import com.andrognito.patternlockview.listener.PatternLockViewListener;
import com.andrognito.patternlockview.utils.PatternLockUtils;

import java.util.List;

public class Schema2 extends AppCompatActivity {

    PatternLockView pattern2;
    private PatternLockViewListener mPatternLockViewListener2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schema2);

        pattern2 = (PatternLockView) findViewById(R.id.schema2);
        mPatternLockViewListener2 = new PatternLockViewListener() {
            @Override
            public void onStarted() {

            }

            @Override
            public void onProgress(List<PatternLockView.Dot> progressPattern) {

            }

            @Override
            public void onComplete(List<PatternLockView.Dot> pattern) {
                Log.d(getClass().getName(), "Pattern progress: " +
                        PatternLockUtils.patternToString(pattern2, pattern));
                SharedPreferences preferences = getSharedPreferences("PEPS1", 0);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString("password", PatternLockUtils.patternToString(pattern2, pattern));
                editor.putString("schema2", PatternLockUtils.patternToString(pattern2, pattern));
                editor.apply();
                Intent intent = new Intent(getApplicationContext(), Creer.class);
                startActivity(intent);
                finish();
            }

            @Override
            public void onCleared() {

            }
        };

        pattern2.addPatternLockListener(mPatternLockViewListener2);
    }
}
