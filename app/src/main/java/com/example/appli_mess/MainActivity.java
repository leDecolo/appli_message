package com.example.appli_mess;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Handler handle = new Handler();
        handle.postDelayed(new Runnable() {
            @Override
            public void run() {
                SharedPreferences preferences = getSharedPreferences("PEPS1", 0);
                String password = preferences.getString("password", "0");

                if (password.equals("0")){
                    Intent intent = new Intent(getApplicationContext(), Creer.class);
                    startActivity(intent);
                    finish();
                } else {

                    Intent intent = new Intent(getApplicationContext(), Entrer_Schema.class);
                    startActivity(intent);
                    finish();
                }
            }
        }, 1000);
    }
}
