package com.example.appli_mess.connexion;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import com.example.appli_mess.beans.Personne;

import java.util.ArrayList;
import java.util.List;

public class Access1 extends SQLiteOpenHelper {
    public static final String nom = "messagerieDB";
    String message = "messages";

    public Access1(Context context){
        super(context, nom, null, 1);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        String creation = "CREATE TABLE dernierMessage (numero integer PRIMARY KEY autoincrement" +
                ", adresse TEXT not null" +
                ", contenu TEXT not null" +
                ", date TEXT not null" +
                ", nonLu integer not null" +
                ", securite integer not null" +
                ", nom TEXT not null)";
        String creation2 = "CREATE TABLE message (" +
                "   numero INTEGER PRIMARY KEY AUTOINCREMENT" +
                ", contenu TEXT not null" +
                ", adresse TEXT not null" +
                ", direction TEXT not null" +
                ", date TEXT not null" +
                ", lecture integer not null)";
        db.execSQL(creation);
        db.execSQL(creation2);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    /*public void addPersonne(Personne pers){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues value = new ContentValues();
        value.put("numero", pers.getNumero());
        value.put("nom", pers.getNom());
        value.put("prenom", pers.getPrenom());
        value.put("sexe", pers.getSexe());
        db.insert("person", null, value );
        db.close();

    }

    public Personne getPerson(String i){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query("person", new String[]{"numero", "nom", "prenom", "sexe"}, "numero = ?",
                new String[]{i}, null, null, null);
        if (cursor != null){
            cursor.moveToFirst();
        }
         Personne pers = new Personne(cursor.getString(1), cursor.getString(2), cursor.getString(0), cursor.getString(3));
        return pers;
    }

    public List<Personne> listPerson(){
        List<Personne> listP = new ArrayList<>();
        String req = "SELECT * FROM person";
        SQLiteDatabase bd = this.getReadableDatabase();
        Cursor cursor = bd.rawQuery(req, null);
        if(cursor.moveToFirst()){
            do {
                Personne p = new Personne(cursor.getString(1), cursor.getString(2), cursor.getString(0), cursor.getString(3));
                listP.add(p);
            } while (cursor.moveToNext());
        }
        return listP;

    }*/
}
