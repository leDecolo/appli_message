package com.example.appli_mess;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.menu.MenuPopupHelper;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.appli_mess.Adapter.messageAdapter;
import com.example.appli_mess.Controle.Cryptage;
import com.example.appli_mess.Controle.Decryptage;
import com.example.appli_mess.Controle.alertDecryptage;
import com.example.appli_mess.Controle.requete_message;
import com.example.appli_mess.beans.Message;
import com.example.appli_mess.beans.dernierMessage;
import com.example.appli_mess.connexion.Access1;
import com.google.android.material.snackbar.Snackbar;

import java.lang.reflect.Field;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Pattern;

import static java.lang.Boolean.TRUE;

public class Esp_Mess extends AppCompatActivity implements messageAdapter.ItemClickListener {

    public static final int DEUXIEME = Menu.FIRST+1;
    public static final int TROISIEME = Menu.FIRST+2;
    public static final int QUATRIEME = Menu.FIRST+3;

    ArrayAdapter<String> array;
    EditText num_tel, mess_texte;
    Map<String, String> contact_num;
    Spinner spin;
    String nom, numero;
    List<Message> listMessage;
    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;
    Access1 access;
    messageAdapter messageadapter;
    int spinnerPosition;
    RelativeLayout esp_btn;
    Timer t;
    Map<Character, Integer> alpha_num;
    Map<Integer, Character> num_alpha;

    private String type_cryptage = "neant";
    private String valeur_cryptage = "";

    RadioButton check;

    char[][] codage1, codage2;

    TextView mes_c;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_esp__mess);
        check = (RadioButton) findViewById(R.id.checkbox_cryp);
        codage1 = new char[26][26];
        codage2 = new char[26][26];

        creation_tab creation =  new creation_tab();
        creation.run();

        esp_btn = (RelativeLayout) findViewById(R.id.esp_btn_supprimer);
        t = new Timer();
        t.schedule(new MonAction(), 0, 2*1000);
        access = new Access1(getApplicationContext());
        //je cree une liste de contact
        contact_num = new HashMap<>();
        layoutManager = new LinearLayoutManager(this);
        recyclerView = (RecyclerView) findViewById(R.id.ecran_message);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
        num_tel = (EditText) findViewById(R.id.numero_tel);
        mess_texte = (EditText) findViewById(R.id.message_texte);
        spin = (Spinner) findViewById(R.id.spinner2);
        recup_numero();
        num_tel.setText(nom);
        listMessage = presenter_message(access, numero);
        messageadapter = new messageAdapter(getApplicationContext(), listMessage);
        messageadapter.setClickListener(this);
        recyclerView.setAdapter(messageadapter);

        messageadapter.setClickListener(this);



        spin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //ici je recupere la position de l'element selectionne
                spinnerPosition = ((ArrayAdapter)spin.getAdapter()).getPosition("contact");
                // maintenant je me rassure d'abord que dans ma liste de contact je ne selectionne pas "contact"
                if(!spin.getSelectedItem().toString().equals("contact")){
                    // je casse le contacte selectionne car il est sous la forme (nom-numero)
                    String[] nom = spin.getSelectedItem().toString().split("-");
                    System.out.println("nom: "+nom[0]+" numero: "+nom[1]);
                    // j'insere maintenant cela dans ma liste de contact en me rassurant d'abord que le text field qui affiche les contacts est soit vide ou pas
                    if(num_tel.getText().toString().equals("")){
                        num_tel.setText(nom[0]);
                        contact_num.put(nom[0], nom[1]);
                        spin.setSelection(spinnerPosition);
                    } else {
                        num_tel.setText(num_tel.getText().toString() + " ; " + nom[0]);
                        contact_num.put(nom[0], nom[1]);
                        spin.setSelection(spinnerPosition);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }

    public void monBtnEnvoyer (View view){
        String message_envoye;
        if (ActivityCompat.shouldShowRequestPermissionRationale(Esp_Mess.this, Manifest.permission.SEND_SMS) == TRUE){
            Snackbar.make(view, "pour envoyer votre sms vous devez d'abord activer la permission sur l'envoi des messages", Snackbar.LENGTH_LONG).setAction("Activer", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ActivityCompat.requestPermissions(Esp_Mess.this, new String[]{Manifest.permission.SEND_SMS}, 100);
                }
            }).show();
        } else {
            //dans ce if je verifie que le champs numero de tel et l'espace du contenu de message ont ete bien remplis
            if (num_tel.getText().toString().trim().equals("") || mess_texte.getText().toString().trim().equals("")) {
                //ici, si les champs n'ont pas ete bien remplis j'affiche une boite de dialogue
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("Veuillez entrer le message et/ou le numero de telephone");
                builder.setTitle("erreur de parametre");
                builder.setPositiveButton("OK", null);
                builder.setCancelable(true);
                builder.create().show();
            } else {
                message_envoye = mess_texte.getText().toString();
                if (type_cryptage.equals("cesar") && check.isChecked())
                    message_envoye = Cryptage.Cryptage_cesar(message_envoye, Integer.parseInt(valeur_cryptage), alpha_num, num_alpha);
                else if (type_cryptage.equals("vigenere") && check.isChecked())
                    message_envoye = Cryptage.Cryptage_Vigenere(message_envoye, valeur_cryptage, codage1);
                System.out.println(message_envoye);
                //a ce niveau, je suis sur que les champs ont ete bien rempli
                //je recupere la date du jour et l'heure
                DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date date = new Date();
                // j'initialise l'objet permettant l'envoi des messages
                SmsManager smsmanager = SmsManager.getDefault();

                // je cree un tableau contenant tous les noms des numeros dont je veux envoyer les messages je sais ici que la position du nom dans (num_tel) correspond a la position du numero dans (list_contact)
                String[] nom1 = num_tel.getText().toString().split(";");

                //l'envoi des messages depend de la longueur ou taille du message a envoyer
                // je teste si la longueur est inferieure a 160 caracteres
                if (mess_texte.getText().toString().length() < 160) {
                    // j'initialise les elements qui vont me permettre d'etablire l'accuse de reception
                    // l'objet qui vq me dire si le message a ete envoye
                    PendingIntent sentPI = PendingIntent.getBroadcast(this, 0, new Intent("SMS_ENVOYE"), 0);

                    //l'objet qui va me dire si le recepteur a recu le message
                    PendingIntent deliveredPI = PendingIntent.getBroadcast(this, 0, new Intent("SMS_RECU"), 0);
                    // j'initialise un compteur pour pouvoir pointe a chaque fois le nom de la personne dont j'utilise son numero et je prend ces noms dans ma liste de numero (num_tel)
                    int i = 0;
                    //
                    // ici j'envoi le message proprement dit en parcourant la liste des numero
                    // String[] contact = num_tel.getText().toString().split(";");
                    for (String num1 : nom1) {
                        String num = contact_num.get(num1.trim());
                        if (num == null)
                            num = num1.trim();
                        // j'initialise un message
                        Message m = new Message(message_envoye
                                , num, "1", format.format(date), 1);
                        // j'envoi le message
                        smsmanager.sendTextMessage(num, null
                                , message_envoye, sentPI, deliveredPI);
                        // je verifie que le numero est deja dans la base de donnee de la liste de mes derniers messages
                        int result = new requete_message().verifDernierMessage(access, num);
                        // si oui je modifie le dernier message et j'enregistre le message dans la liste de mes messages
                        if (result == 1) {
                            new requete_message().modifDernierMessage(num, message_envoye, format.format(date), access);
                            new requete_message().ajout_message(m, access);
                            i++;
                        }
                        // sinon j'enregistre un nouveau dernier message et une nouvelle liste des messages
                        else {
                            dernierMessage dern = new dernierMessage(num, message_envoye
                                    , format.format(date), 0
                                    , 0, nom1[i].trim());
                            new requete_message().ajout_dernier_mess(dern, access);
                            new requete_message().ajout_message(m, access);
                            i++;
                        }


                    }

                    //nombre de message superieur a 160
                } else {

                    // j'initialise les elements qui vont me permettre d'etablire l'accuse de reception
                    // l'objet qui vq me dire si le message a ete envoye
                    ArrayList<PendingIntent> sentPendingIntents = new ArrayList<>();

                    //l'objet qui va me dire si le recepteur a recu le message
                    ArrayList<PendingIntent> deliveredPIs = new ArrayList<PendingIntent>();                // j'initialise un compteur pour pouvoir pointe a chaque fois le nom de la personne dont j'utilise son numero et je prend ces noms dans ma liste de numero (num_tel)
                    int i = 0;
                    //
                    // ici j'envoi le message proprement dit en parcourant la liste des numero
                    for (String num1 : nom1) {

                        String num = contact_num.get(num1.trim());
                        if (num == null)
                            num = num1.trim();

                        // j'initialise un message
                        Message m = new Message(message_envoye, num, "1", format.format(date), 1);
                        // j'envoi le message
                        ArrayList<String> parts = smsmanager.divideMessage(message_envoye);
                        smsmanager.sendMultipartTextMessage(num, null, parts, sentPendingIntents, deliveredPIs);
                        // je verifie que le numero est deja dans la base de donnee de la liste de mes derniers messages
                        int result = new requete_message().verifDernierMessage(access, num);
                        // si oui je modifie le dernier message et j'enregistre le message dans la liste de mes messages
                        if (result == 1) {
                            new requete_message().modifDernierMessage(num, message_envoye, format.format(date), access);
                            new requete_message().ajout_message(m, access);
                            i++;
                        }
                        // sinon j'enregistre un nouveau dernier message et une nouvelle liste des messages
                        else {
                            dernierMessage dern = new dernierMessage(num, message_envoye
                                    , format.format(date), 0
                                    , 0, nom1[i].trim());
                            new requete_message().ajout_dernier_mess(dern, access);
                            new requete_message().ajout_message(m, access);
                            i++;
                        }


                    }

                }

                listMessage = presenter_message(access, numero);
                messageadapter = new messageAdapter(getApplicationContext(), listMessage);
                recyclerView.setAdapter(messageadapter);


            }
        }
    }

    public void recup_numero() {
        // je recupere les donnees recues
        Intent intent = this.getIntent();
        if (intent == null){
            Toast.makeText(getApplicationContext(), "Les donnees envoyees n'ont pas ete recue", Toast.LENGTH_SHORT).show();
        } else{
            //Bundle bundle = intent.getExtras();
            //String numero = bundle.getString("numero");
            // je recupere le contact
            String numero1 = intent.getExtras().getString("contact");
            // je recupere l'identifiant permettant de me dire si c'est un nouveau message ou pas
            int presence = intent.getExtras().getInt("new");
            //je recupere le nom
            String nom1 = intent.getExtras().getString("nom");
            // je recupere le numero
            String numero2 = intent.getExtras().getString("numero");
            if(nom1.equals("sans nom")) {
                numero = numero1;
                contact_num.put(numero1, numero1);
                nom = numero1;
            }else {
                numero = numero2;
                nom = nom1;
                contact_num.put(nom1, numero2);
            }
            if(presence == 1) {
                spin.setVisibility(View.VISIBLE);
                array = new ArrayAdapter(getApplicationContext(), R.layout.apparence_numero, Program_Activite.numero_nom);
                spin.setAdapter(array);

                //parametrer la liste des contacts pour l'ecriture des nouveaux messages
                //spin.setAdapter();
            }else {
                spin.setVisibility(View.GONE);

            }

           // message = new requete_message(getApplicationContext()).getMessage(numero);
            /*for (int i =0; i < message.size(); i++ )
            System.out.println("mon message: "+ message.get(i).getContenu()+ "adresse: "+ message.get(i).getEmetteur());*/

        }

    }

    public List<Message> presenter_message(Access1 access, String numero) {
        return new requete_message().getMessage(access, numero);
    }


    /*@Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        menu.add(Menu.NONE, Menu.FIRST, Menu.NONE, "Supprimer");
        menu.add(Menu.NONE, DEUXIEME, Menu.NONE, "Supprimer plusieurs");
        menu.add(Menu.NONE, TROISIEME, Menu.NONE, "Decrypter");
        menu.add(Menu.NONE, QUATRIEME, Menu.NONE, "Retour");

    }

    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {
        return super.onContextItemSelected(item);
        switch (item.getItemId()){
            case Menu.FIRST:

        }
    }

    @Override
    public void registerForContextMenu(View view) {
        super.registerForContextMenu(view);

    }*/

    class MonAction extends TimerTask {
        int nbrRepetitions = 3;

        public void run() {
            Intent intent = getIntent();
            if (intent != null){
                int sig = intent.getExtras().getInt("signal");
                if(sig == 1){
                    System.out.println("reception d'une valeur");
                    esp_btn.setVisibility(View.VISIBLE);
                }
            } else {
                System.out.println("aucune valeur");
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_principal, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        //reponse dans onActivityResult
        switch (item.getItemId()){
            case R.id.menu_cryptage:
                Intent intent = new Intent(this, Chiffrement.class);
                startActivityForResult(intent, 1001);
                return true;
            case R.id.menu_supprimer:
                return true;
            case R.id.menu_cacher:
                return true;
            default:
                return false;

        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == 1001 && resultCode == 1002){
            if (data.hasExtra("choix"))
                type_cryptage = data.getExtras().getString("choix");
            if (data.hasExtra("valeur")){
                    valeur_cryptage = data.getExtras().getString("valeur");
            }
        }

    }


    @Override
    protected void onResume() {
        super.onResume();

    }

    class creation_tab extends Thread{
        @Override
        public void run() {
            String alpha = "abcdefghijklmnopqrstuvwxyz";
            int decalage = 0;
            int decalage1 = 26;
            for (int i=0; i<26; i++){
                int dec = decalage;
                int dec1 = decalage1;
                for (int j=0; j<26; j++){
                    if (dec1 == 26)
                        dec1 = 0;
                    codage1[i][j] = alpha.charAt(dec);
                    codage2[i][j] = alpha.charAt(dec1);
                    dec++;
                    dec1++;
                    if (dec == 26)
                        dec = 0;
                }
                decalage++;
                decalage1--;
            }

        }
    }


    @Override
    public void onItemClick(final View itemView, final int position, final TextView mes, final TextView date, final Message mes1, final RelativeLayout globe, final int taille) {
        PopupMenu popu = new PopupMenu(globe.getContext(), itemView);
        popu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()){
                    case R.id.i_supprimer:
                        new requete_message().supprimer_message(new Access1(itemView.getContext())
                                , date.getText().toString()
                                , mes.getText().toString()
                                , mes1.getAdresse(), position, taille);
                        globe.setVisibility(View.GONE);
                        return true;

                    case R.id.i_supprimer_p:
                        return TRUE;

                    case R.id.i_decrypter:
                        mes_c = mes;
                        alertDecryptage decrypt = new alertDecryptage();
                        decrypt.show(getSupportFragmentManager(), "decryptage");

                        return true;
                    default:
                        return false;
                }
            }
        });

        popu.inflate(R.menu.menu_esp_mes);
        // popu.setGravity(Gravity.RIGHT);
        try {
            Field mFieldPopup=popu.getClass().getDeclaredField("mPopup");
            mFieldPopup.setAccessible(true);
            MenuPopupHelper mPopup = (MenuPopupHelper) mFieldPopup.get(popu);
            // mPopup.setForceShowIcon(true);
        } catch (Exception e) {

        }

        popu.show();
    }

    @Override
    public void applyTexts(String type, String valeur) {
        if (type.equals("vigenere")){
            String code = valeur;
            String resultat = Decryptage.decryptage_vigenere(mes_c.getText().toString(), code, codage2);
            mes_c.setText(resultat);
        } else if (type.equals("cesar")){
            int val = Integer.parseInt(valeur);
            String resultat = Decryptage.decryptage_cesar(mes_c.getText().toString(), val);
            mes_c.setText(resultat);
        }else{
            AlertDialog.Builder builder = new AlertDialog.Builder(getApplicationContext());
            builder.setMessage("Veuillez entrer une valeur");
            builder.setTitle("Erreur de saisie decalage Cesar");
            builder.setCancelable(true);
            builder.create().show();
        }
    }

}
