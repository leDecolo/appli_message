package com.example.appli_mess.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.appli_mess.Esp_Mess;
import com.example.appli_mess.MainActivity;
import com.example.appli_mess.R;
import com.example.appli_mess.beans.dernierMessage;
import com.example.appli_mess.lesInterfaces.Click_Sur_Elt;

import java.util.List;

public class ListMessageAdapter extends RecyclerView.Adapter<ListMessageAdapter.viewHolder> {
    List<dernierMessage> derniermessage;
    Click_Sur_Elt elt;

    public ListMessageAdapter(List<dernierMessage> derniermessage, Click_Sur_Elt elt) {
        this.derniermessage = derniermessage;
        this.elt = elt;
    }

    @NonNull
    @Override
    public viewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.listmessage, parent, false);
        return new ListMessageAdapter.viewHolder(view, parent.getContext());
    }

    @Override
    public void onBindViewHolder(@NonNull viewHolder holder, int position) {
        dernierMessage derniermess = derniermessage.get(position);
        holder.textMessage.setText(derniermess.getContenu());
        holder.nonlu.setText(Integer.toString(derniermess.getNbreNonLu()));
        if(derniermess.getNom().equals("sans nom")){
            holder.contact.setText(derniermess.getAdresse());
        } else {
            holder.contact.setText(derniermess.getNom());

        }
        holder.numero = derniermess.getAdresse();
        holder.nom = derniermess.getNom();

    }

    @Override
    public int getItemCount() {
        return derniermessage.size();
    }

    public class viewHolder extends RecyclerView.ViewHolder{
        LinearLayout line;
        TextView contact, nonlu, textMessage;
        String nom, numero;

        public viewHolder(@NonNull View itemView, final Context context) {
            super(itemView);
            line = (LinearLayout) itemView.findViewById(R.id.mon_espace_clic);
            contact = (TextView) itemView.findViewById(R.id.nom_contact);
            nonlu = (TextView) itemView.findViewById(R.id.nbre_mes_nonlu);
            textMessage = (TextView) itemView.findViewById(R.id.dernier_message);

            line.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(context, "mon texte "+contact.getText(), Toast.LENGTH_SHORT).show();
                    System.out.println("mon texte "+contact.getText());
                    Intent intent = new Intent(context, Esp_Mess.class);
                    intent.putExtra("contact", contact.getText());
                    intent.putExtra("nom", nom);
                    intent.putExtra("numero", numero);
                    intent.putExtra("new", 0);
                    context.startActivity(intent);

                }
            });
        }
    }
}
