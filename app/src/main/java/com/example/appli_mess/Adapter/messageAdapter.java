package com.example.appli_mess.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.res.XmlResourceParser;
import android.os.Build;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDialogFragment;
import androidx.appcompat.view.menu.MenuPopupHelper;
import androidx.appcompat.view.menu.MenuView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.appli_mess.Controle.Decryptage;
import com.example.appli_mess.Controle.alertDecryptage;
import com.example.appli_mess.Controle.requete_message;
import com.example.appli_mess.Esp_Mess;
import com.example.appli_mess.R;
import com.example.appli_mess.beans.Message;
import com.example.appli_mess.connexion.Access1;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import static java.lang.Boolean.TRUE;

public class messageAdapter extends RecyclerView.Adapter<messageAdapter.ViewHolder> {
    private Context context;
    // ici jai la liste de tous les messages
    List<Message> lesMessage;
    // ici jai la liste de tous les messages entrants
    List<Message> lesMessagesEntrants;
    // ici j'ai la liste de tous les messages sortants
    List<Message> lesMessagesSortants;
    // ici jai la liste de tous les holder ou elements presents sur mon adapter
    List<ViewHolder> holder;
    // ici j'ai toujours la position courante de ma liste
    int pos;
    // ici j'ai toujours le message courant avec tous les parametres
    Message mes1;
    //j'ai la taille de ma liste
    int taille;
    View view1;
    // je declare mon interface
    ItemClickListener itemClickListener;

    public messageAdapter(List<Message> lesMessage, List<Message> lesMessagesEntrants, List<Message> lesMessagesSortants) {
        this.lesMessage = lesMessage;
        this.lesMessagesEntrants = lesMessagesEntrants;
        this.lesMessagesSortants = lesMessagesSortants;
    }

    public messageAdapter(Context context, List<Message> lesMessage) {
        this.lesMessage = lesMessage;
        this.context = context;
        holder = new ArrayList<>();
    }

    @NonNull
    @Override
    public messageAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = null;
       // view1 = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_esp__mess, parent, false);

            if (viewType == 1){
                view = LayoutInflater.from(context).inflate(R.layout.chat_moi, parent, false);
            } else {
                view = LayoutInflater.from(context).inflate(R.layout.chat_autre, parent, false);
            }

        return new messageAdapter.ViewHolder(view, parent.getContext());
    }

    @Override
    public void onBindViewHolder(@NonNull messageAdapter.ViewHolder holder, int position) {
        this.holder.add(holder);
        pos = this.holder.size();
        taille = lesMessage.size();
        mes1 = lesMessage.get(position);
        holder.mes.setText(mes1.getContenu());
        holder.date.setText(mes1.getDate());
        holder.image.setImageResource(R.drawable.logo4);

    }

    @Override
    public int getItemCount() {
        return lesMessage.size();
    }

    public String getEntrntsSortant(){
        return lesMessagesEntrants.size()+"-"+lesMessagesSortants;
    }

    public void setClickListener(ItemClickListener listen){
        this.itemClickListener = listen;

    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        RelativeLayout globe, esp_btn;
        ImageView image;
        TextView mes, date;
        Button btn_sup;
        int pos1;
        Context context;
        View itemV;

        public ViewHolder(@NonNull final View itemView, final Context context) {
            super(itemView);
            itemV = itemView;
            this.context = context;
            globe = (RelativeLayout) itemView.findViewById(R.id.globe_message);
            btn_sup = (Button) itemView.findViewById(R.id.btn_sup1);
            image = (ImageView) itemView.findViewById(R.id.imageProfile);
            mes = (TextView) itemView.findViewById(R.id.textMessage);
            date = (TextView) itemView.findViewById(R.id.idDate);
            btn_sup = (Button) itemView.findViewById(R.id.btn_sup1);
            pos1 = pos;
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            if (itemClickListener != null) itemClickListener.onItemClick(v, getAdapterPosition(), mes, date, mes1, globe, taille);
        }

       /* @Override
        public void applyTexts(String type, String valeur) {
            if (type.equals("vigenere")){

            } else if (type.equals("cesar")){
                int val = Integer.parseInt(valeur);
                String resultat = Decryptage.decryptage_cesar(mes.getText().toString(), val);
                mes.setText(resultat);
            }else{
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setMessage("Veuillez entrer une valeur");
                builder.setTitle("Erreur de saisie decalage Cesar");
                builder.setCancelable(true);
                builder.create().show();
            }
        }*/

    }

    @Override
    public int getItemViewType(int position) {
        if(lesMessage.get(position).getDirection().equals("1")) return 1;
        else return 0;
    }

    public interface ItemClickListener extends alertDecryptage.alertDecryptageListener {
        void onItemClick(View view, int position, TextView t, TextView date, Message mes1, RelativeLayout globe, int taille);
    }

}
