package com.example.appli_mess;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.content.Intent;
import android.os.Bundle;
import android.renderscript.ScriptGroup;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

public class Chiffrement extends AppCompatActivity {
    private RadioGroup choix_chif;
    private RadioButton choix_cesar, choix_vigenere;
    EditText valeur_cesar, valeur_vigenere;
    LinearLayout esp_cesar, esp_vigenere;
    Button btn_valider;
    private int choix = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chiffrement);

        choix_chif = (RadioGroup) findViewById(R.id.choix_chiffrement);
        choix_cesar = (RadioButton) findViewById(R.id.c_cesar);
        choix_vigenere = (RadioButton) findViewById(R.id.c_vigenere);
        valeur_cesar = (EditText) findViewById(R.id.valeur_cesar);
        valeur_vigenere = (EditText) findViewById(R.id.valeur_vigenere);
        btn_valider = (Button) findViewById(R.id.btn_val_chif);
        esp_cesar = (LinearLayout) findViewById(R.id.esp_val_cesar);
        esp_vigenere = (LinearLayout) findViewById(R.id.esp_val_vig);

        choix_chif.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.c_cesar){
                    esp_cesar.setVisibility(View.VISIBLE);
                    esp_vigenere.setVisibility(View.GONE);
                    choix = 1;

                } else {
                    esp_cesar.setVisibility(View.GONE);
                    esp_vigenere.setVisibility(View.VISIBLE);
                    choix = 2;
                }
            }
        });

    }

    @Override
    public void finish() {
        if (choix == 1){
            Intent data = new Intent();
            data.putExtra("choix", "cesar");
            data.putExtra("valeur", valeur_cesar.getText().toString());
            // Activity finished ok, return the data
            setResult(1002, data);
        } else{
            Intent data = new Intent();
            data.putExtra("choix", "vigenere");
            data.putExtra("valeur", valeur_vigenere.getText().toString());
            // Activity finished ok, return the data
            setResult(1002, data);
        }
        super.finish();
    }

    public void monBtnChif(View view) {
        if (choix == 1){
            if (valeur_cesar.getText().toString().equals("")){
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("Veuillez remplir la valeur de decalage de chiffrement par Cesar");
                builder.setTitle("Erreur de saisie decalage Cesar");
                builder.setCancelable(true);
                builder.create().show();
            } else
                finish();
        } else {
            if (valeur_vigenere.getText().toString().equals("")){
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("Veuillez remplir le mot de cryptage du chiffrement par vigenere");
                builder.setTitle("Erreur de saisie mot de vigenere");
                builder.setCancelable(true);
                builder.create().show();
            } else
                finish();

        }

    }
}
